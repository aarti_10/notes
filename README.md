## Notes App ##
Create a Python-Django web application for taking notes. Which should look like the following images.

*Source code can be found [here](https://bitbucket.org/aarti_10/notes/src)*

#### Application workflow ####
* The application provides to manage notes. 
* It provide facilities to create, update and delete notes.
* After deleting note the dashboard should get refresh with valid notes.

#### Tech Stack ####
- Python 
- Django
- MySQL
- HTML
- JQuery

#### Configuration ###
- we need to create virtual environment 
    ```
    $ sudo apt-get install python-virtualenv
    $ virtualenv venv
    $ source  venv/bin/source
    ```
#### Installation ####
- Required packages are listed in requirements.txt. To install them use following command.
    ```
    $  pip install -r requirements.txt
    ```
- To install MySQL Server, refer this [link](https://dev.mysql.com/doc/refman/5.7/en/installing.html). 

#### Database Configuration ####
* Start MySQL server.
    ```
    $ start mysql server 
    ```    
* Log into mysql user.
    ```
    $ mysql -u root -p
    ```    
* Create datbase with name notes_db.
    ```
    mysql>  CREATE DATABASE notes_db;
    ```    
* Create datbase user.
    ```
    mysql>  CREATE USER 'user1'@'localhost' IDENTIFIED BY 'user123';
    ```    
* Give all necessary permissions to the user. 
    ```
    mysql>  GRANT ALL PRIVILEGES ON movies_db.* TO 'user1'@'localhost';
    ```    
### Deployment instructions ###
* To create the tables in database, use below commands -
    ```
    $ python manage.py makemigrations notes_app
    $ python manage.py migrate
    ```
* To start the application
     ```
    $ python manage.py runserver
    ```
* Verify the application by accessing URL http://127.0.0.1:8000

