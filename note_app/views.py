# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
import json
import pytz
import datetime
import logging

from django.template import loader
from django.http.response import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from note_app.models import Notes

logger = logging.getLogger('exception')

@csrf_exempt
def get_all_notes(request):
    """ It will show all the notes

    Params
        request (Request)
            HTTP request

    Returns
        HTTP Response

    """
    local = pytz.timezone("Asia/Kolkata")
    t = loader.get_template('note_app/index.html')
    all_notes = Notes.objects.all().order_by('-created_at')
    notes_list = []
    for note in all_notes:
        note_data = dict()
        note_data["title"] = note.title[:35]
        note_data["description"] = note.description[:30]
        note_data["id"] = note.id
        if note.modified_at.day == datetime.datetime.now().day:
            note_data["modified_at"] = note.modified_at.astimezone(local).strftime('%-I:%M %p')
        else:
            note_data["modified_at"] = note.modified_at.astimezone(local).strftime('%d/%m/%y')
        notes_list.append(note_data)
    c = {'data': notes_list}
    response = HttpResponse(t.render(c))
    return response


@csrf_exempt
def get_note(request):
    """ It will show the note detail

    Params
        request (Request)
            HTTP request

    Returns
        HTTP Response

    """
    if request.method == 'GET':
        id = request.GET.get('id', '')
        note = Notes.objects.get(id=id)
        note_dict = note.to_dict()
        data = json.dumps(note_dict)

    else:
        data = 'fail'
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)

@csrf_exempt
def create_note(request):
    """ It will create a new note

    Params
        request (Request)
            HTTP request

    Returns
        HTTP Response

    """
    try:
        local = pytz.timezone("Asia/Kolkata")
        if request.method == 'POST':
            data = json.loads(request.body)
            note = Notes(title=data.get('title'), description=data.get('desc'))
            note.save()
            data = dict()
            data["title"] = note.title[:35]
            data["description"] = note.description[:30]
            data["id"] = note.id
            if note.modified_at.day == datetime.datetime.now().day:
                data["modified_at"] = note.modified_at.astimezone(local).strftime('%-I:%M %p')
            else:
                data["modified_at"] = note.modified_at.astimezone(local).strftime('%d/%m/%y')
        else:
            data = {}
        mimetype = 'application/json'
    except Exception, e:
        logger.debug(e)
    return HttpResponse(json.dumps(data), mimetype)


@csrf_exempt
def update_note(request):
    """ It update the note

    Params
        request (Request)
            HTTP request

    Returns
        HTTP Response

    """

    result = {}
    if request.method == 'PUT':
        data = json.loads(request.body)
        notes = Notes.objects.filter(id=data['id'])
        if notes:
            note = notes[0]
            note.title = data.get('title', '')
            note.description = data.get('description', '')
            note.save()
            result = note.to_dict()
    mimetype = 'application/json'

    return HttpResponse(json.dumps(result), mimetype)

@csrf_exempt
def delete_note(request):
    """ It update the note

    Params
        request (Request)
            HTTP request

    Returns
        HTTP Response

    """
    if request.method == 'DELETE':
        data = json.loads(request.body)
        try:
            Notes.objects.get(id=data['id']).delete()
        except Exception, e:
            logger.debug("Error in deleting note -> "+str(data['id']))
            logger.debug(e)
    mimetype = 'application/json'
    return HttpResponse(json.dumps({'status': True}), mimetype)



