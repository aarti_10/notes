# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import pytz
from django.db import models

# Create your models here.


from django.db import models

class Notes(models.Model):
    """
        It stores the notes
    """
    title = models.CharField(max_length=100)
    description = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.title

    def to_dict(self):
        local = pytz.timezone("Asia/Kolkata")
        data = {
            'title': self.title,
            'description': self.description,
            'modified_at': self.modified_at.astimezone(local).strftime('%-d %B %Y at %-I:%M %p'),
        }
        return data

    class Meta:
        db_table = 'notes'
        app_label = 'note_app'