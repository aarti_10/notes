function saveNote() {
    var note_data = $("#note_data").val();
    var id = $("#note_data").data('id');
    var index = note_data.indexOf("\n");
    var title = '';
    var description = '';
    if (index != -1) {
        if (index > 100) {
            title = note_data.substring(0, 99)
            description = note_data.substring(100)
        }
        else {
            title = note_data.substring(0, index)
            description = note_data.substring(index)
        }
    }
    if (id != undefined) {
        var data = {'id': id, 'title': title, 'description': description}
        $.ajax({
            url: '/update_note/',
            type: 'put',
            data: JSON.stringify(data),
            success: function (result) {
                $("#new_note").value = data.title + data.description
            },
            error: function () {
                console.log("Error while updating the note");
            }
        });
    }
}

$(function () {
    var t = setInterval(saveNote, 5000);
    setTimeout(function () {
        $(".notes_list li:first").trigger("click");
    }, 10 );

    $(".notes_list").on("click", "li", function(event){
    // $("li").click(function(){
        $("li").removeClass("active");
        $(this).addClass("active");
        $(".menubar li").removeClass("active");


        var id = $(this).data('id');
        console.log(id)

        $("#note_date").text('');
        $("#note_data").val('');
        $("#note_data").data('id', id);
        if (id != undefined) {
            $.ajax({
                url: '/get_note/?id=' + id,
                type: 'get',
                success: function (data) {

                    $("#note_date").text(data.modified_at);

                    $("#note_data").val(data.title + data.description);

                },
                error: function () {
                    console.log("Error while fetching the note");
                }
            });

        }
    });
    $( "#new_note" ).click(function() {
        $("#note_data").val("");
        var data = {"title": "", "desc": ""}
        $.ajax({
            url: '/create_note/',
            type: 'post',
            dataType: 'json',
            data: JSON.stringify(data),
            success: function (data) {

                $("#note_data").data("id",data.id);
                var title = data.title != "" ? data.title : 'New Note';
                var desc = data.description != "" ? data.desc : 'No additional text';

                  $("li").removeClass("active");

                var new_element ='<li id="note-' + data.id + '" class="notes_element active" data-id="' + data.id + '">' ;
                    new_element += '<div>  <p class="title">' + title + '</p>';
                    new_element += '<p>' + data.modified_at + ' &nbsp; &nbsp; <span class="description">' + desc + '</p> </div> </li>';
                $(".notes_list").prepend(new_element);
            },
            error: function () {
                console.log("Error while creating the note");
        }
        });
    });

    $( "#delete_note" ).click(function() {
        var id = $("#note_data").data("id");

        var data = {"id": id}
        $.ajax({
            url: '/delete_note/',
            type: 'delete',
            dataType: 'json',
            data: JSON.stringify(data),
            success: function () {
                $("#note-" + id).remove();
                $(".notes_list li:first").trigger("click");
            },
            error: function () {
                console.log("Error while creating the note");
            }
        });
    });

    $("#note_data").bind('input propertychange', function() {

        var id  = $("#note_data").data('id');
        console.log(id)

        var note_data = $("#note_data").val();
        console.log(note_data)
        var index = note_data.indexOf("\n");
        var title = '';
        var description = '';
        if (index != -1) {
            if (index > 100) {
                title = note_data.substring(0, 99)
                description = note_data.substring(100)
            }
            else {
                title = note_data.substring(0, index)
                description = note_data.substring(index)
            }
        }

        if(title.length > 30) {
            $("#note-" + id + " .title").text(title.substr(0, 29) + "...");
        }else {
            $("#note-" + id + " .title").text(title);
        }
        if(description.length > 30) {
            $("#note-" + id + " .description").text(description.substr(0, 29) + "...");
        }else {
            $("#note-" + id + " .description").text(description);
        }
    });
});